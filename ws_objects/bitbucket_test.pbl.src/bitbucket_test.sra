﻿$PBExportHeader$bitbucket_test.sra
$PBExportComments$Generated Application Object
forward
global type bitbucket_test from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables

end variables
global type bitbucket_test from application
string appname = "bitbucket_test"
end type
global bitbucket_test bitbucket_test

on bitbucket_test.create
appname = "bitbucket_test"
message = create message
sqlca = create transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on bitbucket_test.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

