﻿$PBExportHeader$w_test.srw
forward
global type w_test from window
end type
end forward

global type w_test from window
integer width = 4160
integer height = 1760
boolean titlebar = true
string title = "タイトル未設定"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
end type
global w_test w_test

on w_test.create
end on

on w_test.destroy
end on

